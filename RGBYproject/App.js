/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.urutan}>
          <View style={styles.merah}>
              <Text style={styles.text}>R</Text>
          </View>

          <View style={styles.hijau}>
              <Text style={styles.text}>G</Text>
          </View>
        </View>

          <View style={styles.urutan}>

            <View style={styles.biru}>
              <Text style={styles.text}>B</Text>
            </View>

            <View style={styles.kuning}>
              <Text style={styles.text}>Y</Text>
            </View>
          </View>

        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  urutan:{
    flex: 1,
    flexDirection: 'row'
  },
  text: {
    fontSize: 70,
    textAlign: 'center',
    margin: 10,
    color: 'white'
  },
  merah: {
    flex: 1,
    backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',

    },
    hijau: {
        flex: 1,
        backgroundColor: 'green',
        justifyContent: 'center',
        alignItems: 'center',

    },
    biru: {
        flex: 1,
        backgroundColor: 'blue',
        justifyContent: 'center',
        alignItems: 'center',

    },
    kuning: {
        flex: 1,
        backgroundColor: 'yellow',
        justifyContent: 'center',
        alignItems: 'center',

    },
});
